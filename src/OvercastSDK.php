<?php
namespace MindOfMicah\OvercastSDK;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class Crawler extends DomCrawler 
{
    public function getText(string $selector, string $attribute = '_text'): string
    {
        return trim($this->filter($selector)->eq(0)->extract($attribute)[0]);
    }
}

class OvercastSDK
{
    private $login_response;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
        $this->client = new Client([
            'cookies' => true,
            'base_uri'=>'https://overcast.fm',
        ]);
    }

    private function get(string $url, array $params = [], array $options = []): Crawler
    {
        return $this->request('get', $url, $params, $options);
    }
    private function post(string $url, array $params = [], array $options = []): Crawler
    {
        return $this->request('post', $url, $params, $options);
    }

    private function request(string $method, string $url, array $params = [], array $options = []): Crawler
    {
        return new Crawler($this->requestRaw($method, $url, $params, $options));
    }

    private function requestRaw(string $method, string $url, array $params = [], array $options = []): string
    {
        if ($params) {
            $options['form_params'] = $params;
        }

        try {
            ($this->client->request($method, $url, $options));
        } catch (\Exception $e) {
            throw $e;
        dd($url);
            dd($e->getResponse());
        }

        return (string)$this->client->request($method, $url, $options)->getBody();
    }

    private function login()
    {
        if ($this->login_response) {
            return $this->login_response;
        }

        return $this->login_response = $this->post('login', [
            'email'    => $this->username,
            'password' => $this->password,
        ]);
    }

    public function getFeedEpisodes(string $feed_id)
    {
        $this->login();
        return $this->get($feed_id)->filter('.extendedepisodecell')->each(function (Crawler $crawler){
            $is_played = strpos($crawler->attr('class'), 'usernewepisode') === false;
            $id = ltrim($crawler->attr('href'), '/');
            $title = $crawler->getText('.title');
            $description = $crawler->getText('.lighttext');
    
            return compact('id', 'is_played', 'title', 'description');
        });
    }

    public function getEpisodeDetails(string $episode_id)
    {
        $this->login();    

        $crawler = $this->get($episode_id);

        return  [
            'audio_src' =>strtok($crawler->getText('#audioplayer > source', 'src'), '#')
        ];
    }

    public function getFeeds()
    {
        return $this->login()->filter('.feedcell')->each(function (Crawler $crawler) {
            return [
                'artwork'      => $crawler->getText('.art', 'src'),
                'title'        => $crawler->getText('.title'),
                'id'           => trim($crawler->attr('href'), '/'),
                'has_unplayed' => !!$crawler->filter('svg.unplayed_indicator')->count(),
            ];
        });
    }

    public function getFeedId(array $wheres = [])
    {
        return current(array_filter($this->login()->filter('.feedcell')->each(function (Crawler $crawler) use ($wheres) {
            if ($wheres['title'] == $crawler->getText('.title')) {
                return trim($crawler->attr('href'), '/');
            }

            return false;
        })));
    }

    public function getEpisodes()
    {
        return $this->login()->filter('.episodecell')->each(function (Crawler $crawler) {
            $img_source = $this->getTextFromCrawler($crawler, 'img', 'src');
            $author = $this->getTextFromCrawler($crawler, '.caption2');
            $title = $this->getTextFromCrawler($crawler, '.title');
            $id = $crawler->extract('href')[0];

            return compact('img_source', 'author', 'title', 'id');
        });
    }

    public function deleteEpisode()
    {

    }

    private function getTextFromCrawler(Crawler $crawler, string $selector, string $attribute = '_text'): string
    {
        return trim($crawler->filter($selector)->eq(0)->extract($attribute)[0]);
    }

    public function subscribeToPodcast($podcast_id)
    {
        $listings = (string)$this->client->post('https://overcast.fm/login', [
            'form_params' => [
                'then'     => $podcast_id,
                'email'    => $this->username,
                'password' => $this->password,
            ],
        ])->getBody();

        $crawler = new Crawler($listings);
        dd(get_defined_vars(), __FILE__, __LINE__);
    }

    public function unsubscribeFromPodcast($podcast_id)
    {
        $page = (string)$this->client->post('https://overcast.fm/login', [
            'form_params' => [
                'then'     => $podcast_id,
                'email'    => $this->username,
                'password' => $this->password,
            ],
        ])->getBody();

        $a = $this->client->get('https://overcast.fm/' . $podcast_id);
        $b = (string)$a->getBody();

        $crawler = new Crawler($b);
        unset($a, $b, $page);

        $link = $crawler->filter('#deletepodcastform')->eq(0)->attr('action');
        $response = (string)$this->client->post('https://overcast.fm/' . ltrim($link, '/'))->getBody();
    }

    public function fetchSummary()
    {
        $this->login();
        sleep(5);
        return $this->requestRaw('get', '/account/export_opml/extended');
    }
}
